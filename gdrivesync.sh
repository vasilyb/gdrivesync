#!/bin/sh

pull() {
	local _definition="${1}"
	if test -n "${_definition}"
	then
		local path=`_get_definition_path "${_definition}"`
		test_definition "${path}"
		pull_one "${path}" "${_definition}"
	else
		pull_all
	fi
}

pull_one() {
	local _path="${1}"
	local _definition="${2}"
	local backup_key='source_backup_dir'
	local backup=`extract_definition_value "${_path}" "$backup_key"`
	local source_key='source_local_dir'
	local source=`extract_definition_value "${_path}" "$source_key"`
	local local_key='drive_local_dir'
	local local=`extract_definition_value "${_path}" "$local_key"`
	test_drive_local_dir "${local}"
	local remote_key='drive_remote_dir'
	local remote="`extract_definition_value "${_path}" "$remote_key"`"
	backup_local_dir "$backup" "$_path" "$source" "$_definition"
	local zip="`pull_latest_archive "$local" "$remote" "$_definition"`"
	empty_local_dir "${source}"
	unpack_archive_locally "${source}" "${local}" "${zip}"
	remove_latest_archive "${zip}" "${local}"
}

pull_all() {
	local path="`_get_definition_path '*'`"
	test_definitions "${path}"
	local definitions="$(ls ${path})"
	for path in ${definitions}
	do
		local definition="`get_definition_name "${path}"`"
		pull_one "${path}" "${definition}"
	done
}

backup_local_dir() {
	_dir="${1}"
	_path="${2}"
	_source="${3}"
	_definition="${4}"
	if test -z "${_dir}"
	then
		return
	fi
	mkdir -p "${_dir}"
	local archive=`pack_local_dir "${_source}"`
	local list="`mktemp -u`"
	find "${_dir}" -type f -name ${_definition}*.zip > "${list}"
	local name="`_get_archive_path "${_dir}" "${_definition}"`"
	enumerate_latest_local_backup "$list" "$_dir" "$_definition" "$name"
	rm "${list}"
	mv "${archive}" "${name}"
}

enumerate_latest_local_backup() {
	_list="${1}"
	_dir="${2}"
	_definition="${3}"
	_name="${4}"
	test_if_should_enumerate "${_list}" "${_dir}" "${_definition}"
	should=$?
	if test "$should" -eq 0
	then
		local next=`get_next_archive_index "${_list}"`
		local new="${_dir}/${_definition}-${next}.zip"
		mv "${_name}" "${new}"
	fi
}

pull_latest_archive() {
	local _local="${1}"
	local _remote="${2}"
	local _definition="${3}"
	local list="`mktemp -u`"
	local dir="`pwd`"
	cd "${_local}"
	drive ls "${_remote}" > "${list}"
	cd "${dir}"
	remote_has_unenumerated_archive "${list}" "${_remote}" ; has=$?
	if test "${has}" -eq 0
	then
		local archive="`pull_unenumerated_latest_archive \
			"${_remote}" "${_definition}" "${_local}"`"
	else
		local archive="`pull_enumerated_latest_archive \
			"${list}" "${_remote}" "${_definition}" "${_local}"`"
	fi
	rm "${list}"
	echo "${archive}"
}

remote_has_unenumerated_archive() {
	local _list="${1}"
	local _remote="${2}"
	local _definiiton="${3}"
	test_if_should_enumerate "${_list}" "${_remote}" "${_definition}"
	local should=$?
	return "${should}"
}

pull_unenumerated_latest_archive() {
	local _remote="${1}"
	local _definition="${2}"
	local _local="${3}"
	local path="`_get_archive_path "${_remote}" "${_definition}"`"
	pull_archive_by_its_path "${_local}" "${path}"
	echo "${path}"
}

pull_enumerated_latest_archive() {
	local _list="${1}"
	local _remote="${2}"
	local _definition="${3}"
	local _local="${4}"
	local max="`get_max_archive_index "${_list}"`"
	local path="`_get_archive_path "$remote" "$definition" "$max"`"
	pull_archive_by_its_path "${_local}" "${path}"
	echo "${path}"
}

pull_archive_by_its_path() {
	local _local="${1}"
	local _path="${2}"
	local dir="`pwd`"
	cd "${_local}"
	drive pull -quiet "${_path}"
	cd "${dir}"
}

empty_local_dir() {
	local _source="${1}"
	rm -rf ${_source}/*
}

unpack_archive_locally() {
	local _source="${1}"
	local _local="${2}"
	local _zip="${3}"
	local dir="`dirname "${_source}"`"
	local wd="`pwd`"
	cd "${_local}"
	unzip -qu -d "${dir}" "${_zip}"
	cd "${wd}"
}

remove_latest_archive() {
	local _archive="${1}"
	local _local="${2}"
	local wd="`pwd`"
	cd "${_local}"
	rm "${_archive}"
	cd "${wd}"
}

push() {
	local _definition="${1}"
	if test -n "${_definition}"
	then
		local path=`_get_definition_path "${_definition}"`
		test_definition "${path}"
		push_one "${path}" "${_definition}"
	else
		push_all
	fi
}

push_one() {
	local _path="${1}"
	local _definition="${2}"
	local local_key='drive_local_dir'
	local local=`extract_definition_value "${_path}" "$local_key"`
	test_drive_local_dir "${local}"
	local source_key='source_local_dir'
	local source=`extract_definition_value "${_path}" "$source_key"`
	local archive=`pack_local_dir "${source}"`
	local remote_key='drive_remote_dir'
	local remote="`extract_definition_value "${_path}" "$remote_key"`"
	local latest="`_get_archive_path "${remote}" "${_definition}"`"

	enumerate_latest_remote \
		"${_path}" "${local}" "${latest}" "${remote}" "${definition}"

	cd "${local}"
	mkdir -p "${remote}"
	mv -f "${archive}" "${latest}"
	drive push -quiet -force "${latest}"
	rm -f "${latest}"
	cd -
}

push_all() {
	local path="`_get_definition_path '*'`"
	test_definitions "${path}"
	local definitions="$(ls ${path})"
	for path in ${definitions}
	do
		local definition="`get_definition_name "${path}"`"
		push_one "${path}" "${definition}"
	done
}

test_definition() {
	local _path="${1}"
	if ! test -f "${path}"
	then
		echo "No definition found at '${path}'"
		exit ${ERROR_DEFINITION_NOT_FOUND}
	fi
}

test_definitions() {
	local _path="${1}"
	ls ${path} 2> /dev/null > /dev/null ; exist=$?
	if test "${exist}" != '0'
	then
		echo "No definitions found: '${_path}'"
		exit ${ERROR_NO_DEFINITIONS_FOUND}
	fi
}

get_definition_name() {
	_path="${1}"
	echo "`basename -s '.conf' "${_path}"`"
}

extract_definition_value() {
	local _path="${1}"
	local _key="${2}"
	echo `sed -En 's/^'"${_key}"'="([^"]*)"/\1/ip' "${_path}"`
}

pack_local_dir() {
	local _source="${1}"
	local archive="$(mktemp -u).zip"
	cd "$(dirname "${source}")"
	zip -r9q "${archive}" "./$(basename "${source}")"
	cd -
	echo "${archive}"
}

enumerate_latest_remote() {
	local _path="${1}"
	local _local="${2}"
	local _archive="${3}"
	local _remote="${4}"
	local _definition="${5}"

	cd "${_local}"
	local list="`mktemp -u`"
	drive ls "${_remote}" > "${list}"
	local definition="`get_definition_name "${_path}"`"
	test_if_should_enumerate "${list}" "${_remote}" "${definition}"
	local should=$?
	if test "${should}" -eq 0
	then
		local next=`get_next_archive_index "${list}"`
		local name="${definition}-${next}.zip"
		drive rename "${_archive}" "${name}"
	fi
	rm "${list}"
	cd -
}

test_drive_local_dir() {
	local _path="${1}"
	cd "${_path}"
	drive about > /dev/null 2> /dev/null ; inited=$?
	cd -
	if test "${inited}" != 0
	then
		echo "Drive context not found in '${_path}'"
		exit ${ERROR_NO_CONTEXT_FOUND}
	fi
}

get_max_archive_index() {
	local _list="${1}"
	local max="`sed -En 's/.*-([0-9]+)\.zip/\1/ip' "${_list}" \
		| sort -g | tail -n 1`"
	echo "${max}"
}

get_next_archive_index() {
	local _list="${1}"
	local next="$((`get_max_archive_index "${list}"` + 1))"
	echo "${next}"
}

test_if_should_enumerate() {
	local _list="${1}"
	local _path="${2}"
	local _definition="${3}"

	local re="${_path}/${_definition}\.zip\$"
	local count="`egrep -c -- "${re}" "${_list}"`"

	if test "${count}" -eq 1
	then
		return 0
	else
		return 1
	fi
}

unknown() {
	local _command="${1}"
	echo "Unknown command: '${_command}'"
	exit ${ERROR_COMMAND_UNKNOWN}
}

_get_definition_path() {
	local _definition="${1}"
	echo "${CONFIG_SUB_DIR}/${_definition}.conf"
}

_get_archive_path() {
	local _dir="${1}"
	local _definition="${2}"
	local _number="${3}"
	local number="${_number:+"-${_number}"}"
	echo "${_dir}/${_definition}${number}.zip"
}

__command="${1}"
__definition="${2}"

ERROR_COMMAND_UNKNOWN=1
ERROR_DEFINITION_NOT_FOUND=2
ERROR_NO_DEFINITIONS_FOUND=3
ERROR_NO_CONTEXT_FOUND=4

CONFIG_SUB_DIR="$HOME/.config/gdrivesync"

case "$__command" in
	pull) pull "${__definition}";;
	push) push "${__definition}";;
	*) unknown "${__command}";;
esac
