# gdrivesync

A shell script for FreeBSD to sync and backup directories using the drive program. Manually, i.e. not auto-sync.

## How does it work

### `push`

This script:

1. packs your files from each `local_dir` (see below) to a ZIP archive (as most popular); from the sample config below it will create the `cornerstone.zip` file from the `/local/path/` directory;
1. renames `cornerstone.zip` to `cornerstone-N.zip`, where `N = M+1` and `M` is the maximum number of `/Google/Drive/path/cornerstone-M.zip`;
1. uploads local `cornerstone.zip` to `remote_dir`;
1. removed local `cornerstone.zip`

### `pull`

1. Renames `cornerstone.zip` in `backup_dir` to `cornerstone-N.zip` where `N = M+1` and `M` is the maximal number in `/local/path/where/to/store/backups/cornerstone-M.zip`
1. Creates `cornerstone.zip` from `local_dir`
1. Puts `cornerstone.zip` to `backup_dir`
1. Clears `local_dir`
1. Downloads `cornerstone.zip` from `remote_dir`
1. Unpacks downloaded `cornerstone.zip` to `local_dir`

### Privacy

This script uses the `drive` program to access your Google Drive to
1. push ZIP archives to it,
1. pull ZIP archives from it.

As this script is open-source software you may look through the code to ensure that the script
1. uses only the remote directory on your drive that is specified in the definitions,
1. does not use other directories,
1. not aimed to stole your credentials,
1. not aimed to stole your data from Google Drive,
1. not aimed to spoil or remove your data from Google Drive,
1. not aimed to spy for your nor read your data.
1. This script does not sends your data to third parties anyhow.

Therefore this script is just an _agent_ between your local directories you want to backup and the Google Drive and uses the `drive` program just like a backend for Google Drive.

## Usage

### Setup and authenticate with `drive`

Do

```sh
sudo pkg install drive
mkdir ~/drive/ # where you want the drive program to work with your Google Drive
cd ~/drive/
drive init
```

and follow the instructions.

### Download the script

### Configure it

Configuration location: `~/.config/gdrivesync/`.

Configuration of `gdrivesync` is a set of _definitions_.
Each definition sits in the configuration directory mentioned above.

E.g. a content of `~/.config/gdrivesync/cornerstone.conf` definiiton may look like:

```
source_local_dir="/local/path/"
source_backup_dir="/local/path/where/to/store/backups/"
drive_local_dir="/local/path/to/dir/where/your/drive/inited"
drive_remote_dir="/remote/dir/where/archives/stored"
```

* `source_local_dir` is a local directory containing files you want to sync. An obligatory setting
* `drive_local_dir` is a path in Google Drive from which you want to pull files using the `drive` program. This setting is obligatory, obviously
* `drive_remote_dir` is a path for the `drive` program to find a remote directory in your Google Drive where you want `gdrivesync` to drop archives to using the `push` command and get archives from using the `pull` command of `gdrivesync`. No files will be stored here: the script uses this directory just for credentials.
**NB:** Do not use a leading slash (`/`) in this parameter to make the `gdrivesync` to work properly!
* `source_backup_dir` is an optional (but strictly recommended) setting to set a local directory where `gdrivesync` will store backups created automatcally before each `pull`.
  This will save you from overwriting and therefore loosing your local changes after preliminary `pull`

**NB:** Don't use `~` to point the `$HOME`, use absolute path, e.g. `/home/jdoe/.config/my.conf`.
The script won't work if you set any dir in your home directory with the tilde like this: `~/.config/my.conf`.
You may try to use `"${HOME}"` though, but I'm unsure if this will work. Test it and tell me ;)

Example:

```
source_local_dir="/home/vblinkov/.config/cornerstone"
source_backup_dir=""
drive_local_dir="/home/vblinkov/gdrive"
drive_remote_dir="system/cornerstone"
```

### Use it

Note: you are able to push or pull all configuration sources (in square brackets in the configuration file) or a single one.

#### Push

```
./gdrivesync push[ cornerstone]
```

#### Pull

```
./gdrivesync pull[ cornerstone]
```

## Development

This is a small shell script aimed to port the functionality of the [CornerWin](https://gitlab.com/vasilyb/cornerstone/-/tree/master/contrib/cornerwin) to sync [Cornerstone](https://gitlab.com/vasilyb/cornerstone) data under FreeBSD.

### To-do

-[ ] FreeBSD port
